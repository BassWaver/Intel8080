#![allow(non_snake_case)]


use std::fs::File;
use std::path::Path;
use std::io::Read;
use std::io::prelude::*;


pub fn decompile(path: &String) {
    let mut file = File::open(Path::new(path)).expect("fuck off hitler");
    let mut raw: Vec<u8> = Vec::new();
    file.read_to_end(&mut raw);
    let buf: Vec<String> = buildbuf(raw);
    write(construct(buf), &String::from("source/decompiled"));
}

fn buildbuf(raw:Vec<u8>) -> Vec<String> {
    let mut buf: Vec<String> = Vec::new();
    for i in raw {
        buf.push(format!("{:x}", i));
    }
    return buf;
}

fn construct(buf: Vec<String>) -> Vec<String> {
    let mut ass: Vec<String> = Vec::new();
    let mut i: usize = 0;
    while i<buf.len(){
        let mut t:String = String::new();
        match &buf[i][..] {
            "1"=>  {
                i+=2;
                t=format!("LXI B,#${}{}", buf[i], buf[i-1]);
            }
            "2"=>  {
                t="STAX B".to_string();;
            }
            "3"=>  {
                t="INX B".to_string();;
            }
            "4"=>  {
                t="INR B".to_string();;
            }
            "5"=>  {
                t="DCR B".to_string();;
            }
            "6"=>  {
                i+=1;
                t=format!("MVI B,#${}", buf[i]);
            }
            "7"=>  {
                t="RCL".to_string();;
            }
            "9"=>  {
                t="DAD B".to_string();;
            }
            "a"=>  {
                t="LDAX B".to_string();;
            }
            "b"=>  {
                t="DCX B".to_string();;
            }
            "c"=>  {
                t="INR C".to_string();;
            }
            "d"=>  {
                t="DCR C".to_string();;
            }
            "e"=>  {
                i+=1;
                t=format!("MVI C,#${}", buf[i]);
            }
            "f"=>  {
                t="RRC".to_string();;
            }
            "11"=>  {
                i+=2;
                t=format!("LXI D,#${}{}", buf[i], buf[i-1]);
            }
            "12"=>  {
                t="STAX D".to_string();;
            }
            "13"=>  {
                t="INX D".to_string();;
            }
            "14"=>  {
                t="INR D".to_string();;
            }
            "15"=>  {
                t="DCR D".to_string();;
            }
            "16"=>  {
                i+=1;
                t=format!("MVI D,#${}", buf[i]);
            }
            "17"=>  {
                t="RAL".to_string();;
            }
            "19"=>  {
                t="DAD D".to_string();;
            }
            "1a"=>  {
                t="LDAX D".to_string();;
            }
            "1b"=>  {
                t="DCX".to_string();;
            }
            "1c"=>  {
                t="INR E".to_string();;
            }
            "id"=>  {
                t="DCR E".to_string();;
            }
            "ie"=>  {
                i+=1;
                t=format!("MVI E#${}", buf[i]);
            }
            "if"=>  {
                t="RAR".to_string();;
            }
            "20"=>  {
                t="RIM".to_string();;
            }
            "21"=>  {
                i+=2;
                t=format!("LXI H#${}{}", buf[i], buf[i-1]);
            }
            "22"=>  {
                i+=2;
                t=format!("SHID ${}{}", buf[i], buf[i-1]);
            }
            "23"=>  {
                t="INX H".to_string();;
            }
            "24"=>  {
                t="INR H".to_string();;
            }
            "25"=>  {
                t="DCR H".to_string();;
            }
            "26"=>  {
                i+=1;
                t=format!("MVI H#${}", buf[i]);
            }
            "27"=>  {
                t="DAA".to_string();;
            }
            "29"=>  {
                t="DAD H".to_string();;
            }
            "2a"=>  {
                i+=2;
                t=format!("LHLD ${}{}", buf[i], buf[i-1]);
            }
            "2b"=>  {
                t="DCX H".to_string();;
            }
            "2c"=>  {
                t="INR L".to_string();;
            }
            "2d"=>  {
                t="DCR L".to_string();;
            }
            "2e"=>  {
                i+=1;
                t=format!("MVI L,#${}", buf[i]);
            }
            "2f"=>  {
                t="CMA".to_string();;
            }
            "30"=>  {
                t="SIM".to_string();;
            }
            "31"=>  {
                i+=2;
                t=format!("LXI SP,#${}{}", buf[i], buf[i-1]);
            }
            "32"=>  {
                i+=2;
                t=format!("STA ${}{}", buf[i], buf[i-1]);
            }
            "33"=>  {
                t="INX SP".to_string();;
            }
            "34"=>  {
                t="INR M".to_string();;
            }
            "35"=>  {
                t="DCR M".to_string();;
            }
            "36"=>  {
                i+=1;
                t=format!("MVI M,#${}", buf[i]);
            }
            "37"=>  {
                t="STC".to_string();;
            }
            "39"=>  {
                t="DAD SP".to_string();;
            }
            "3a"=>  {
                i+=2;
                t=format!("LDA ${}{}", buf[i], buf[i-1]);
            }
            "3b"=>  {
                t="DCX SP".to_string();
            }
            "3c"=>  {
                t="INR A".to_string();
            }
            "3d"=>  {
                t="DCR A".to_string();
            }
            "3e"=>  {
                i+=1;
                t=format!("MVI A,#${}", buf[i]);
            }
            "3f"=>  {
                t="CMC".to_string();
            }
            "40"=>  {
                t="MOV B,B".to_string();
            }
            "41"=>  {
                t="MOV B,C".to_string();
            }
            "42"=>  {
                t="MOV B,D".to_string();
            }
            "43"=>  {
                t="MOV B,E".to_string();
            }
            "44"=>  {
                t="MOV B,H".to_string();
            }
            "45"=>  {
                t="MOV B,L".to_string();
            }
            "46"=>  {
                t="MOV B,M".to_string();
            }
            "47"=>  {
                t="MOV B,A".to_string();
            }
            "48"=>  {
                t="MOV C,B".to_string();
            }
            "49"=>  {
                t="MOV C,C".to_string();
            }
            "4a"=>  {
                t="MOV C,D".to_string();
            }
            "4b"=>  {
                t="MOV C,E".to_string();
            }
            "4c"=>  {
                t="MOV C,H".to_string();
            }
            "4d"=>  {
                t="MOV C,L".to_string();
            }
            "4e"=>  {
                t="MOV C,M".to_string();
            }
            "4f"=>  {
                t="MOV C,A".to_string();
            }
            "50"=>  {
                t="MOV D,B".to_string();
            }
            "51"=>  {
                t="MOV D,C".to_string();
            }
            "52"=>  {
                t="MOV D,D".to_string();
            }
            "53"=>  {
                t="MOV D,E".to_string();
            }
            "54"=>  {
                t="MOV D,H".to_string();
            }
            "55"=>  {
                t="MOV D,L".to_string();
            }
            "56"=>  {
                t="MOV D,M".to_string();
            }
            "57"=>  {
                t="MOV D,A".to_string();
            }
            "58"=>  {
                t="MOV E,B".to_string();
            }
            "59"=>  {
                t="MOV E,C".to_string();
            }
            "5a"=>  {
                t="MOV E,D".to_string();
            }
            "5b"=>  {
                t="MOV E,E".to_string();
            }
            "5c"=>  {
                t="MOV E,H".to_string();
            }
            "5d"=>  {
                t="MOV E,L".to_string();
            }
            "5e"=>  {
                t="MOV E,M".to_string();
            }
            "5f"=>  {
                t="MOV E,A".to_string();
            }
            "60"=>  {
                t="MOV H,B".to_string();
            }
            "61"=>  {
                t="MOV H,C".to_string();
            }
            "62"=>  {
                t="MOV H,D".to_string();
            }
            "63"=>  {
                t="MOV H,E".to_string();
            }
            "64"=>  {
                t="MOV H,H".to_string();
            }
            "65"=>  {
                t="MOV H,L".to_string();
            }
            "66"=>  { // Execute Order 66
                t="MOV H,M".to_string();
            }
            "67"=>  {
                t="MOV H,A".to_string();
            }
            "68"=>  {
                t="MOV L,B".to_string();
            }
            "69"=>  {
                t="MOV L,C".to_string();
            }
            "6a"=>  {
                t="MOV L,D".to_string();
            }
            "6b"=>  {
                t="MOV L,E".to_string();
            }
            "6c"=> {
                t="MOV L,H".to_string();
            }
            "6d"=>  {
                t="MOV L,L".to_string();
            }
            "6e"=>  {
                t="MOV L,M".to_string();
            }
            "6f"=>  {
                t="MOV L,A".to_string();
            }
            "70"=>  {
                t="MOV M,B".to_string();
            }
            "71"=>  {
                t="MOV M,C".to_string();
            }
            "72"=>  {
                t="MOV M,D".to_string();
            }
            "73"=>  {
                t="MOV M,E".to_string();
            }
            "74"=>  {
                t="MOV M,H".to_string();
            }
            "75"=>  {
                t="MOV M,L".to_string();
            }
            "76"=>  {
                t="HLT".to_string();
            }
            "77"=>  {
                t="MOV M,A".to_string();
            }
            "78"=>  {
                t="MOV A,B".to_string();
            }
            "79"=>  {
                t="MOV A,C".to_string();
            }
            "7a"=>  {
                t="MOV A,D".to_string();
            }
            "7b"=>  {
                t="MOV A,E".to_string();
            }
            "7c"=>  {
                t="MOV A,H".to_string();
            }
            "7d"=>  {
                t="MOV A,L".to_string();
            }
            "7e"=>  {
                t="MOV A,M".to_string();
            }
            "7f"=>  {
                t="MOV A,F".to_string();
            }
            "80"=>  {
                t="ADD B".to_string();
            }
            "81"=>  {
                t="ADD C".to_string();
            }
            "82"=>  {
                t="ADD D".to_string();
            }
            "83"=>  {
                t="ADD E".to_string();
            }
            "84"=>  {
                t="ADD H".to_string();
            }
            "85"=>  {
                t="ADD L".to_string();
            }
            "86"=>  {
                t="ADD M".to_string();
            }
            "87"=>  {
                t="ADD A".to_string();
            }
            "88"=>  {
                t="ADC B".to_string();
            }
            "89"=>  {
                t="ADC C".to_string();
            }
            "8a"=>  {
                t="ADC D".to_string();
            }
            "8b"=>  {
                t="ADC E".to_string();
            }
            "8c"=>  {
                t="ADC H".to_string();
            }
            "8d"=>  {
                t="ADC L".to_string();
            }
            "8e"=>  {
                t="ADC M".to_string();
            }
            "8f"=>  {
                t="ADD A".to_string();
            }
            "90"=>  {
                t="SUB B".to_string();
            }
            "91"=>  {
                t="SUB C".to_string();
            }
            "92"=>  {
                t="SUB D".to_string();
            }
            "93"=>  {
                t="SUB E".to_string();
            }
            "94"=>  {
                t="SUB H".to_string();
            }
            "95"=>  {
                t="SUB L".to_string();
            }
            "96"=>  {
                t="SUB M".to_string();
            }
            "97"=>  {
                t="SUB A".to_string();
            }
            "98"=>  {
                t="SBB B".to_string();
            }
            "99"=>  {
                t="SBB C".to_string();
            }
            "9a"=>  {
                t="SBB D".to_string();
            }
            "9b"=>  {
                t="SBB E".to_string();
            }
            "9c"=>  {
                t="SBB H".to_string();
            }
            "9d"=>  {
                t="SBB L".to_string();
            }
            "9e"=>  {
                t="SBB M".to_string();
            }
            "9f"=>  {
                t="SBB A".to_string();
            }
            "a0"=>  {
                t="ANA B".to_string();
            }
            "a1"=>  {
                t="ANA C".to_string();
            }
            "a2"=>  {
                t="ANA D".to_string();
            }
            "a3"=>  {
                t="ANA E".to_string();
            }
            "a4"=>  {
                t="ANA H".to_string();
            }
            "a5"=>  {
                t="ANA L".to_string();
            }
            "a6"=>  {
                t="ANA M".to_string();
            }
            "a7"=>  {
                t="ANA A".to_string();
            }
            "a8"=>  {
                t="XRA B".to_string();
            }
            "a9"=>  {
                t="XRA C".to_string();
            }
            "aa"=>  {
                t="XRA D".to_string();
            }
            "ab"=>  {
                t="XRA E".to_string();
            }
            "ac"=>  {
                t="XRA H".to_string();
            }
            "ad"=>  {
                t="XRA L".to_string();
            }
            "ae"=>  {
                t="XRA M".to_string();
            }
            "af"=>  {
                t="XRA A".to_string();
            }
            "b0"=>  {
                t="ORA B".to_string();
            }
            "b1"=>  {
                t="ORA C".to_string();
            }
            "b2"=>  {
                t="ORA D".to_string();
            }
            "b3"=>  {
                t="ORA E".to_string();
            }
            "b4"=>  {
                t="ORA H".to_string();
            }
            "b5"=>  {
                t="ORA L".to_string();
            }
            "b6"=>  {
                t="ORA M".to_string();
            }
            "b7"=>  {
                t="ORA A".to_string();
            }
            "b8"=>  {
                t="CMP B".to_string();
            }
            "b9"=>  {
                t="CMP C".to_string();
            }
            "ba"=>  {
                t="CMP D".to_string();
            }
            "bb"=>  {
                t="CMP E".to_string();
            }
            "bc"=>  {
                t="CMP H".to_string();
            }
            "bd"=>  {
                t="CMP L".to_string();
            }
            "be"=>  {
                t="CMP M".to_string();
            }
            "bf"=>  {
                t="CMP A".to_string();
            }
            "c0"=>  {
                t="RNZ".to_string();
            }
            "c1"=>  {
                t="POP B".to_string();
            }
            "c2"=>  {
                i+=2;
                t=format!("JNZ ${}{}", buf[i], buf[i-1]);
            }
            "c3"=>  {
                i+=2;
                t=format!("JMP ${}{}", buf[i], buf[i-1]);
            }
            "c4"=>  {
                i+=2;
                t=format!("CNZ ${}{}", buf[i], buf[i-1]);
            }
            "c5"=>  {
                t="PUSH B".to_string();
            }
            "c6"=>  {
                i+=1;
                t=format!("ADI #${}", buf[i]);
            }
            "c7"=>  { // TODO Check this syntax
                t="RST 0".to_string();
            }
            "c8"=>  {
                t="RZ".to_string();
            }
            "c9"=>  {
                t="RET".to_string();
            }
            "ca"=>  {
                i+=2;
                t=format!("JZ ${}{}", buf[i], buf[i-1]);
            }
            "cc"=>  {
                i+=2;
                t=format!("CZ ${}{}", buf[i], buf[i-1]);
            }
            "cd"=>  {
                i+=2;
                t=format!("CALL ${}{}", buf[i], buf[i-1]);
            }
            "ce"=>  {
                i+=1;
                t=format!("ACI #${}", buf[i]);
            }
            "cf"=>  {
                t="RST 1".to_string();
            }
            "d0"=>  {
                t="RNC".to_string();
            }
            "d1"=>  {
                t="POP D".to_string();
            }
            "d2"=>  {
                i+=2;
                t=format!("JNC ${}{}", buf[i], buf[i-1]);
            }
            "d3"=>  {
                i+=1;
                t=format!("OUT $#{}", buf[i]);
            }
            "d4"=>  {
                i+=2;
                t=format!("CNC ${}{}", buf[i], buf[i-1]);
            }
            "d5"=>  {
                t="PUSH D".to_string();
            }
            "d6"=>  {
                i+=1;
                t=format!("SUI #${}", buf[i]);
            }
            "d7"=>  {
                t="RST 2".to_string();
            }
            "d8"=>  {
                t="RC".to_string();
            }
            "da"=>  {
                i+=2;
                t=format!("JC ${}{}", buf[i], buf[i-1]);
            }
            "db"=>  {
                i+=1;
                t=format!("IN #${}", buf[i]);
            }
            "dc"=>  {
                i+=2;
                t=format!("CC ${}{}", buf[i], buf[i-1]);
            }
            "de"=>  {
                i+=1;
                t=format!("SBI #${}", buf[i]);
            }
            "df"=>  {
                t="RST 3".to_string();
            }
            "e0"=>  {
                t="RPO".to_string();
            }
            "e1"=>  {
                t="POP H".to_string();
            }
            "e2"=>  {
                i+=2;
                t=format!("JPO ${}{}", buf[i], buf[i-1]);
            }
            "e3"=>  {
                t="XHTL".to_string();
            }
            "e4"=>  {
                i+=2;
                t=format!("CPO ${}{}", buf[i], buf[i-1]);
            }
            "e5"=>  {
                t="PUSH H".to_string();
            }
            "e6"=>  {
                i+=1;
                t=format!("ANI #${}", buf[i]);
            }
            "e7"=>  {
                t="RST 4".to_string();
            }
            "e8"=>  {
                t="RPE".to_string();
            }
            "e9"=>  {
                t="PCHL".to_string();
            }
            "ea"=>  {
                i+=2;
                t=format!("JPE ${}{}", buf[i], buf[i-1]);
            }
            "eb"=>  {
                t="XCHG".to_string();
            }
            "ec"=>  {
                i+=2;
                t=format!("CPE ${}{}", buf[i], buf[i-1]);
            }
            "ee"=>  {
                i+=1;
                t=format!("XRI #${}", buf[i]);
            }
            "ef"=>  {
                t="RST 5".to_string();
            }
            "f0"=>  {
                t="RP".to_string();
            }
            "f1"=>  {
                t="POP PSW".to_string();
            }
            "f2"=>  {
                i+=2;
                t=format!("JP ${}{}", buf[i], buf[i-1]);
            }
            "f3"=>  {
                t="DI".to_string();
            }
            "f4"=>  {
                i+=2;
                t=format!("CP ${}{}", buf[i], buf[i-1]);
            }
            "f5"=>  {
                t="PUSH PSW".to_string();
            }
            "f6"=>  {
                i+=1;
                t=format!("ORI #${}", buf[i]);
            }
            "f7"=>  {
                t="RST 6".to_string();
            }
            "f8"=>  {
                t="RM".to_string();
            }
            "f9"=>  {
                t="SPHL".to_string();
            }
            "fa"=>  {
                i+=2;
                t=format!("JM ${}{}", buf[i], buf[i-1]);
            }
            "fb"=>  {
                t="EL".to_string();
            }
            "fc"=>  {
                i+=2;
                t=format!("CM ${}{}", buf[i], buf[i-1]);
            }
            "fe"=>  {
                i+=1;
                t=format!("CPI #${}", buf[i]);
            }
            "ff"=>  {
                t="RST 7".to_string();
            }
            "0"=>   {
                t="NOP".to_string();
            }
            _  =>  {
                t="NOP".to_string();
            }
        }
        ass.push(format!("{}\n", t));
        i+=1
    }
    return ass;
}

fn write(buf: Vec<String>, path: &String) {
    let mut file = File::create(path).expect("failed to create");
    for line in buf {
        file.write(line.as_bytes());
    }
}
